This directory contains the full source for the author's BFS implementation that features a wait-free, retry-free, arbitrary-n persistent thread scheduler.

