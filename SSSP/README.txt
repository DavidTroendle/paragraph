This directory contains the full source for the author's SSSP implementation that features a speculate and correct SSSP algorithm using two wait-free, retry-free, arbitrary-n persistent thread schedulers.

